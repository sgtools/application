import * as fs from 'fs';
import * as path from 'path';
import { exit } from 'process';

interface CommandItem
{
    names       : string[];
    function    : Function;
    help        : string;
}

export class Application
{
    public name         : string;
    public version      : string;
    public description  : string;
    private commands    : CommandItem[];
    private default     : Function;
    private onExit      : () => boolean;

    constructor()
    {
        this.commands = [];

        let pathFile = path.join(__dirname, '../../../..', 'package.json');
        let data = fs.readFileSync(pathFile, 'utf8');
        let json = JSON.parse(data);
        this.name = json.name;
        this.version = json.version;
        this.description = json.description;
    }

    public setName(name: string)
    {
        this.name = name;
    }

    public setDescription(description: string)
    {
        this.description = description;
    }

    public addCommand(names: string | string[], cb: Function, help: string)
    {
        this.commands.push({
            names       : (typeof names === 'string' ? new Array(names) : names),
            function    : cb,
            help        : help
        });
    }

    public getCommands(): { names: string[], help: string }[]
    {
        return this.commands.map(command => { return { names: command.names, help: command.help }; });
    }

    public addDefault(cb: Function)
    {
        this.default = cb;
    }

    public addOnExit(cb: () => boolean)
    {
        this.onExit = cb;
    }

    public exit(): boolean
    {
        if (this.onExit) return this.onExit();
        return true;
    }

    public exitFn()
    {
        return () => { return app.exit(); }
    }




    private printVersion()
    {
        console.info(`${this.name} version ${this.version}`);
    }
    
    private printHelp(full: boolean = true)
    {
        //TODO
        console.info('Usage: kit [f|folders]');
        if (full) {
            console.info('');
            console.info('');
        }
    }

    private printCommandHelp(command: CommandItem)
    {
        console.info(`Usage: ${this.name} ${command.names[0]} ${command.help}`);
    }




    public run()
    {
        // HANDLE VERSION
        if (process.argv.length === 3 && process.argv[2] === '--version') {
            this.printVersion();
            exit(1);
        }
        
        // HANDLE HELP
        if (process.argv.length === 3 && process.argv[2] === '--help') {
            this.printHelp();
            exit(1);
        }

        process.argv.splice(0, 2);

        // HANDLE PARAMS

        /*
        let param = process.argv[0];
        if (param.startsWith('-')) {

        }
*/

        // HANDLE COMMAND
        let cmd = process.argv[0];

        let hasRun = false;
        this.commands.forEach(command => {
            if (command.names.includes(cmd)) {
                hasRun = true;
                process.argv.splice(0, 1);
                let cmdParams = process.argv;
                if (!command.function(cmdParams)) {
                    this.printCommandHelp(command);
                }
            }
        });

        // HANDLE DEFAULT NO PARAM COMMAND
        if (!hasRun && this.default) {
            this.default();
        } else if (!hasRun) {
            this.printHelp();
        }
    }
}


export const app = new Application();
