# Welcome to @sgtools/application
[![Version](https://npm.bmel.fr/-/badge/sgtools/application.svg)](https://npm.bmel.fr/-/web/detail/@sgtools/application)
[![Documentation](https://img.shields.io/badge/documentation-yes-brightgreen.svg)](https://gitlab.com/sgtools/application/-/blob/master/docs/README.md)
[![License: ISC](https://img.shields.io/badge/License-ISC-yellow.svg)](https://spdx.org/licenses/ISC)

> Application framework nodejs applications


## Install

```sh
npm install @sgtools/application
```

## Usage

```sh
import { app } from '@sgtools/application';
```

Code documentation can be found [here](https://gitlab.com/sgtools/application/-/blob/master/docs/README.md).


## Author

**Sébastien GUERRI** <sebastien.guerri@apps.bmel.fr>


## Issues

Contributions, issues and feature requests are welcome!

Feel free to check [issues page](https://gitlab.com/sgtools/application/issues). You can also contact the author.


## License

This project is [ISC](https://spdx.org/licenses/ISC) licensed.
