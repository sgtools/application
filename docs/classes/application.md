[@sgtools/application](../README.md) / [Exports](../modules.md) / Application

# Class: Application

## Hierarchy

* **Application**

## Table of contents

### Constructors

- [constructor](application.md#constructor)

### Properties

- [commands](application.md#commands)
- [default](application.md#default)
- [description](application.md#description)
- [name](application.md#name)
- [onExit](application.md#onexit)
- [version](application.md#version)

### Methods

- [addCommand](application.md#addcommand)
- [addDefault](application.md#adddefault)
- [addOnExit](application.md#addonexit)
- [exit](application.md#exit)
- [exitFn](application.md#exitfn)
- [getCommands](application.md#getcommands)
- [printCommandHelp](application.md#printcommandhelp)
- [printHelp](application.md#printhelp)
- [printVersion](application.md#printversion)
- [run](application.md#run)
- [setDescription](application.md#setdescription)
- [setName](application.md#setname)

## Constructors

### constructor

\+ **new Application**(): [*Application*](application.md)

**Returns:** [*Application*](application.md)

Defined in: index.ts:19

## Properties

### commands

• `Private` **commands**: CommandItem[]

Defined in: index.ts:17

___

### default

• `Private` **default**: Function

Defined in: index.ts:18

___

### description

• **description**: *string*

Defined in: index.ts:16

___

### name

• **name**: *string*

Defined in: index.ts:14

___

### onExit

• `Private` **onExit**: () => *boolean*

Defined in: index.ts:19

___

### version

• **version**: *string*

Defined in: index.ts:15

## Methods

### addCommand

▸ **addCommand**(`names`: *string* \| *string*[], `cb`: Function, `help`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`names` | *string* \| *string*[] |
`cb` | Function |
`help` | *string* |

**Returns:** *void*

Defined in: index.ts:43

___

### addDefault

▸ **addDefault**(`cb`: Function): *void*

#### Parameters:

Name | Type |
------ | ------ |
`cb` | Function |

**Returns:** *void*

Defined in: index.ts:57

___

### addOnExit

▸ **addOnExit**(`cb`: () => *boolean*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`cb` | () => *boolean* |

**Returns:** *void*

Defined in: index.ts:62

___

### exit

▸ **exit**(): *boolean*

**Returns:** *boolean*

Defined in: index.ts:67

___

### exitFn

▸ **exitFn**(): *function*

**Returns:** *function*

Defined in: index.ts:73

___

### getCommands

▸ **getCommands**(): { `help`: *string* ; `names`: *string*[]  }[]

**Returns:** { `help`: *string* ; `names`: *string*[]  }[]

Defined in: index.ts:52

___

### printCommandHelp

▸ `Private`**printCommandHelp**(`command`: CommandItem): *void*

#### Parameters:

Name | Type |
------ | ------ |
`command` | CommandItem |

**Returns:** *void*

Defined in: index.ts:96

___

### printHelp

▸ `Private`**printHelp**(`full?`: *boolean*): *void*

#### Parameters:

Name | Type | Default value |
------ | ------ | ------ |
`full` | *boolean* | true |

**Returns:** *void*

Defined in: index.ts:86

___

### printVersion

▸ `Private`**printVersion**(): *void*

**Returns:** *void*

Defined in: index.ts:81

___

### run

▸ **run**(): *void*

**Returns:** *void*

Defined in: index.ts:104

___

### setDescription

▸ **setDescription**(`description`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`description` | *string* |

**Returns:** *void*

Defined in: index.ts:38

___

### setName

▸ **setName**(`name`: *string*): *void*

#### Parameters:

Name | Type |
------ | ------ |
`name` | *string* |

**Returns:** *void*

Defined in: index.ts:33
