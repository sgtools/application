[@sgtools/application](README.md) / Exports

# @sgtools/application

## Table of contents

### Classes

- [Application](classes/application.md)

### Variables

- [app](modules.md#app)

## Variables

### app

• `Const` **app**: [*Application*](classes/application.md)

Defined in: index.ts:154
