"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.app = exports.Application = void 0;
const fs = require("fs");
const path = require("path");
const process_1 = require("process");
class Application {
    constructor() {
        this.commands = [];
        let pathFile = path.join(__dirname, '../../../..', 'package.json');
        let data = fs.readFileSync(pathFile, 'utf8');
        let json = JSON.parse(data);
        this.name = json.name;
        this.version = json.version;
        this.description = json.description;
    }
    setName(name) {
        this.name = name;
    }
    setDescription(description) {
        this.description = description;
    }
    addCommand(names, cb, help) {
        this.commands.push({
            names: (typeof names === 'string' ? new Array(names) : names),
            function: cb,
            help: help
        });
    }
    getCommands() {
        return this.commands.map(command => { return { names: command.names, help: command.help }; });
    }
    addDefault(cb) {
        this.default = cb;
    }
    addOnExit(cb) {
        this.onExit = cb;
    }
    exit() {
        if (this.onExit)
            return this.onExit();
        return true;
    }
    exitFn() {
        return () => { return exports.app.exit(); };
    }
    printVersion() {
        console.info(`${this.name} version ${this.version}`);
    }
    printHelp(full = true) {
        //TODO
        console.info('Usage: kit [f|folders]');
        if (full) {
            console.info('');
            console.info('');
        }
    }
    printCommandHelp(command) {
        console.info(`Usage: ${this.name} ${command.names[0]} ${command.help}`);
    }
    run() {
        // HANDLE VERSION
        if (process.argv.length === 3 && process.argv[2] === '--version') {
            this.printVersion();
            process_1.exit(1);
        }
        // HANDLE HELP
        if (process.argv.length === 3 && process.argv[2] === '--help') {
            this.printHelp();
            process_1.exit(1);
        }
        process.argv.splice(0, 2);
        // HANDLE PARAMS
        /*
        let param = process.argv[0];
        if (param.startsWith('-')) {

        }
*/
        // HANDLE COMMAND
        let cmd = process.argv[0];
        let hasRun = false;
        this.commands.forEach(command => {
            if (command.names.includes(cmd)) {
                hasRun = true;
                process.argv.splice(0, 1);
                let cmdParams = process.argv;
                if (!command.function(cmdParams)) {
                    this.printCommandHelp(command);
                }
            }
        });
        // HANDLE DEFAULT NO PARAM COMMAND
        if (!hasRun && this.default) {
            this.default();
        }
        else if (!hasRun) {
            this.printHelp();
        }
    }
}
exports.Application = Application;
exports.app = new Application();
