export declare class Application {
    name: string;
    version: string;
    description: string;
    private commands;
    private default;
    private onExit;
    constructor();
    setName(name: string): void;
    setDescription(description: string): void;
    addCommand(names: string | string[], cb: Function, help: string): void;
    getCommands(): {
        names: string[];
        help: string;
    }[];
    addDefault(cb: Function): void;
    addOnExit(cb: () => boolean): void;
    exit(): boolean;
    exitFn(): () => boolean;
    private printVersion;
    private printHelp;
    private printCommandHelp;
    run(): void;
}
export declare const app: Application;
